
package Docker;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class dockerWithChrome {

	@Test
	public static void chromeOpener() throws MalformedURLException {
	 
		ChromeOptions chrome = new ChromeOptions();
		
	    URL url=new URL("http://localhost:4444/wd/hub");
	    RemoteWebDriver driver = new  RemoteWebDriver(url,chrome);
        driver.get("https://www.google.com");
        System.out.println("Title of the page is:" + driver.getTitle());
        driver.quit();
	}

}
